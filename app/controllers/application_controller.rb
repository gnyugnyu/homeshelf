class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception


  # def after_sign_in_path_for(resource)
  #   user_profile_path(resource)
  # end


  def after_sign_in_path_for(resource)
    if resource.sign_in_count == 1
       new_user_profile_path(resource)
    else
       user_profile_path(resource)
    end
  end
end
