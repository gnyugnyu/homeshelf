class BooksController < ApplicationController

  def new
    @book = Book.new
  end

  def create
    @book = Book.create(book_params)
    @copy = @book.copies.build(owner: current_user)

    if @copy.save
      redirect_to user_profile_path(current_user), notice: 'Book has been added to your shelf.'
    else
      render :new
    end
  end

# resources :copies do
#   get  :new, path: 'books/:book_id/new'
#   post :create, path: 'copies/:book_id'
# end
#
# resources :books do
#   get  :new, path: 'copies/:user_id/new'
#   post :create, path: 'books/:copy_id'
# end
  private

  def book_params
    params.require(:book).permit(:title, :author)
  end
end
