class RentalBooksController < ApplicationController

  def new

  end

  def create
    @rental_book = RentalBook.new(book_params)
    if @rental_book.save
      redirect_to user_profile_path(current_user), notice: "Request sent"
    else
      redirect_to user_profile_path(current_user), alert: "Unexpected errors"
    end
  end

  def update
    @rental_book = RentalBook.find(params[:id])
    @rental_book.accepted = true
    if @rental_book.save
      redirect_to user_profile_path(current_user), notice: "Book lent"
    else
      redirect_to user_profile_path(current_user), alert: "Unexpected errors"
    end
  end

  def destroy
    @rental_book = RentalBook.find(params[:id])
    @rental_book.destroy
    redirect_to user_profile_path(current_user), notice: "dismissed"
  end

  private

  def book_params
    params.permit(:user_id, :book_copy_id)
  end
end
