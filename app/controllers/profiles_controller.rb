class ProfilesController < ApplicationController

  before_action :set_profile, only: [:edit, :new, :update]

  def show
    @users = User.all
    @copies = current_user.available_books_to_borrow
    @borrowed_books = current_user.borrowed_books
    @borrow_books = current_user.borrow_books
  end

  def edit
  end

  def new
  end

  def create
    @profile = current_user.build_profile(profile_params)
    if @profile.save
      redirect_to user_profile_path(current_user), notice: 'User profile was successfully created.'
    else
      render :new
    end
  end

  def update
    if @profile.update_attributes(profile_params)
      redirect_to user_profile_path(current_user), notice: 'User profile was successfully updated.'
    else
      render :new
    end
  end

  # def create
  #   @profile = Profile.new(profile_params)
  #   if @profile.save
  #     redirect_to user_profile_path(resource)
  #   else
  #     render :new
  #   end
  # end

  private

  def set_profile
    @profile = current_user.profile
  end

  def profile_params
    params.require(:profile).permit(:name, :avatar)
  end
end
