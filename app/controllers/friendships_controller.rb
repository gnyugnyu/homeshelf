class FriendshipsController < ApplicationController

  def create
    @friendship = current_user.friendships.build(friendship_params)
    if @friendship.save
      redirect_to user_profile_path(current_user), notice: "Friendship requested"
    else
      redirect_to user_profile_path(current_user), alert: "Unable to request friendship"
    end
  end

  def update
    @friendship = User.find(params[:id]).friendships.find_by(friend_id: current_user.id)
    @friendship.accepted = true
    if @friendship.save
      redirect_to root_url, notice: "Successfully confirmed friend!"
    else
      redirect_to root_url, notice: "Sorry! Could not confirm friend!"
    end
  end

  def destroy
    @friendship = Friendship.find(id: params[:id])
    @friendship.destroy
    redirect_to user_profile_path(current_user), notice: "Removed friendship."
  end

  private

  def friendship_params
    params.permit(:friend_id)
  end
end
