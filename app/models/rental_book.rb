class RentalBook < ApplicationRecord
  belongs_to :book_copy
  belongs_to :user
  before_create :set_status

  private

  def set_status
    self.accepted = false
  end

end
