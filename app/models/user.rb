class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many :copies, class_name: "BookCopy", foreign_key: :owner_id
  has_one :profile

  # borrow books
  has_many :borrow_rental_books, -> { where(rental_books: { accepted: false }) }, class_name: "RentalBook"
  has_many :borrowed_rental_books, -> { where(rental_books: { accepted: true }) }, class_name: "RentalBook"
  has_many :borrow_books, through: :borrow_rental_books, source: :book_copy
  has_many :borrowed_books, through: :borrowed_rental_books, source: :book_copy

  # lend books
  has_many :lend_rental_books, through: :copies, class_name: "RentalBook"
  has_many :lend_books, through: :lend_rental_books, source: :book_copy
  has_many :lent_rental_books, through: :copies, class_name: "RentalBook"
  has_many :lent_books, through: :lent_rental_books, source: :book_copy

  has_many :friendships
  has_many :received_friendships, class_name: "Friendship", foreign_key: "friend_id"

  has_many :active_friends, -> { where(friendships: { accepted: true}) }, through: :friendships, source: :friend
  has_many :received_friends, -> { where(friendships: { accepted: true}) }, through: :received_friendships, source: :user
  has_many :pending_friends, -> { where(friendships: { accepted: false}) }, through: :friendships, source: :friend
  has_many :requested_friendships, -> { where(friendships: { accepted: false}) }, through: :received_friendships, source: :user

  def available_copies
    copies - lent_books

  end

  def profile
    super || build_profile
  end

  def friends
    active_friends | received_friends
  end

  def pending
    pending_friends | requested_friendships
  end

  def name
    profile.name || email
  end

  def available_books_to_borrow
    BookCopy.where.not(owner_id: self.id) - borrow_books - borrowed_books
  end


  def available_friends
    User.all.where.not(id: self.id) - friends - pending
  end

end
