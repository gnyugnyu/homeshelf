class Book < ApplicationRecord
  has_many :copies, class_name: "BookCopy" 
  mount_uploader :cover, CoverUploader
end
