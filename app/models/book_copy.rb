class BookCopy < ApplicationRecord
  belongs_to :book
  belongs_to :owner, foreign_key: :owner_id, class_name: "User"
  has_many :rental_books
  has_many :lend_rental_books, -> { where(accepted: false) }, class_name: "RentalBook"
  has_many :lent_rental_books, -> { where(accepted: true) }, class_name: "RentalBook"

  def active_status
    if book_rentals.last.user == owner
      "homeshelf"
    else
      "lent"
    end
  end


end
