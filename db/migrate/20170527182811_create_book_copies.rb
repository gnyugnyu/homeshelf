class CreateBookCopies < ActiveRecord::Migration[5.1]
  def change
    create_table :book_copies do |t|
      t.references :book, foreign_key: true
      t.integer :owner_id
      t.integer :user_id

      t.timestamps
    end
  end
end
