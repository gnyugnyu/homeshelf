class RemoveUserIdFromBookCopy < ActiveRecord::Migration[5.1]
  def change
    remove_column :book_copies, :user_id, :integer
  end
end
