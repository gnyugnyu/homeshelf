class CreateRentalBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :rental_books do |t|
      t.boolean :accepted
      t.integer :book_copy_id
      t.integer :user_id

      t.timestamps
    end
  end
end
