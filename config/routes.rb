Rails.application.routes.draw do
  resources :friendships, only: [:create, :update, :destroy]

  authenticated :user do
    root to: 'profiles#show', as: :authenticated_root
  end

  root to: 'welcome#index'
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }
  resources :books
  resources :users do
    resources :book_copies do
      resources :rental_books
    end
    resource :profile
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
